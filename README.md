#Movie Sentiment Analyzer 

This is a docker web application crated for an assignment

The user enters a movie name, that name is run against the open movie database and matching movies are returned

After the user selects one of the entries the youtube API looks up the trailer and grabs a bunch of comments

The google sentiment analysis API then scores the comments, the average is taken and a score is given and returned to the user along with the movies information