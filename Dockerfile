#Base Image
FROM node:6.3

#File Author/Maintainer
MAINTAINER Ashley McVeigh

RUN mkdir /src

RUN npm install nodemon -g

WORKDIR /src

EXPOSE 3000
