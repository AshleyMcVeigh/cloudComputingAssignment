var express = require('express');
var router = express.Router();

//API wrappers
var omdb = require('omdb');
var youtubeNode = require('youtube-node');
var request = require('request');
var language = require('@google-cloud/language');
var async = require('async');

var googleAPIKey = "AIzaSyCJIldRGf063zaI8g410Gq2pOIUaUUMyYQ";

var languageClient = language({
    projectId: 'agile-athlete-143802',
    keyFilename: "/src/moviesentiment/googleKey.json"
});

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/', function (req, res) {
  console.log(req.body.title);
  //send off to movie database
    omdb.search({terms: req.body.title, type: 'movie'}, function(err, movies) {
        if(err) {
            return console.error(err);
        }
        if(movies.length < 1) {
            return console.log('No movies were found!');
        } else {
            console.log(movies);
            res.render('confirm', {payload: JSON.stringify(movies)});
        }

    });
});


router.post('/movie', function(req, res) {
    //grab movie data based on ID.
    omdb.get({ imdb: req.body.selected }, true, function(err, movie) {
        if(err) {
            return console.error(err);
        }

        if(!movie) {
            return console.log('Movie not found!');
        }

        //search and select first video on youtube.
        var youTube = new youtubeNode();
        youTube.setKey(googleAPIKey);
        youTube.search(movie.title + " trailer official " + movie.year, 1, function(error, result) {
            if (error) {
                console.log(error);
            }
            else {
                //comment request body
                var textFormat = "&textFormat=plainText";
                var part = "&part=snippet";
                var videoId = "&videoId=" + result.items[0].id.videoId;
                var maxResults = "&maxResults=100";
                var requestURL = "https://www.googleapis.com/youtube/v3/commentThreads?key=" + googleAPIKey + textFormat + part + videoId + maxResults;

                //send request
                request.get(requestURL, function(error, response, body) {
                    if (error){
                        console.log(error);
                    }
                    else {
                        //parse comments into array
                        var body = JSON.parse(body);
                        var count = Object.keys(body.items).length;
                        var comments = [];
                        for(var i=0; i<count;i++ ){
                            comments.push(body.items[i].snippet.topLevelComment.snippet.textDisplay);
                        }

                        var fn = function getSentiment(comment){ // sample async action
                            return new Promise( function(resolve, reject){
                                languageClient.detectSentiment(comment, function(err, sentiment) {
                                    if (!err) {
                                        resolve(sentiment);
                                    } else {
                                        console.log(err);
                                        resolve("error")
                                    }
                                });
                            });
                        };
                        var actions = comments.map(fn);
                        var results = Promise.all(actions);
                        results.then(data => {
                            var actualLength = 0;
                            var total = 0;
                            for (var j = 0; j < data.length; j++) {
                                if (parseFloat(data[j])) {
                                    total += parseFloat(data[j]);
                                    actualLength++
                                }
                            }
                            var AVGSentiment = parseFloat(total / actualLength).toFixed(2);

                            var sentimentDesc = "";

                            if (AVGSentiment<=-75){ sentimentDesc = "Abysmal"}
                            else if (AVGSentiment<=-50 && AVGSentiment>-76){ sentimentDesc = "Very Poor"}
                            else if (AVGSentiment<=-25 && AVGSentiment>-51){ sentimentDesc = "Poor"}
                            else if (AVGSentiment<=0 && AVGSentiment>-26){ sentimentDesc = "Mediocre"}
                            else if (AVGSentiment<=25 && AVGSentiment<1){ sentimentDesc = "Average"}
                            else if (AVGSentiment<=50 && AVGSentiment<26){ sentimentDesc = "Good"}
                            else if (AVGSentiment<=75 && AVGSentiment<51){ sentimentDesc = "Great"}
                            else if (AVGSentiment<=100 && AVGSentiment<76){ sentimentDesc = "Fantastic"}

                            res.render('result', {sdesc: sentimentDesc, sentiment: AVGSentiment, movie: movie});
                        });
                    }
                });
            }
        });
    });
});

module.exports = router;
